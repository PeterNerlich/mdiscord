# mDiscord

A responsive, super-leightweight chat client for Discord

> This is a university project. As such, only a limited time is available and only a limited scope is applied. After the term, this project might get abandoned, deleted or extended. During the one term being a university project, there might be a reduced interaction with externally posted issues or pull requests. Please mind that those contributions might get avoided or moderated such as to deliver a clear project for marking, with hardly any of external help. Thank you.

## Goals/Roadmap

Single-page application: **responsive**, super-leightweight chat client for Discord, no voice channels (for now?)

- [X] read messages
- [X] server/channel navigation
- [X] login page (token)
- [X] send messages

Bonus goals:

- [ ] markdown support
- [ ] *new messages* indicator
- [ ] friendly login with email + password (if possible and technologically justifyable)
- [ ] mentions + link support
- [ ] emoticons and "reactions"

## Motivation

The Discord web application is not responsive. On smaller screens, the menu and sidebars cover the message area entirely, proper mobile representation is only available via dedicated native apps. The goal is the creation of an extra lightweight web app to access Discord for the means of chatting, without the need to install and start up another app from the browser and being able to access it at all on less established operating systems, which do not have the support of the Discord developers.

## Technology & Frameworks

Aiming to be lightweight, this project does not use many frameworks besides the standard HTML5/JS APIs (especially DOM, Localstorage, Promises). The only exception is [Discord.js](https://discord.js.org/#/docs/main/stable/topics/web) (web build, [version 11.4.2](https://github.com/discordjs/discord.js/raw/webpack/discord.11.4.2.min.js)), which acts as a wrapper around the Discord API.

## Coding style

Many features and conventions of [ES6](http://es6-features.org/) are advised for this project. Namely, the use of block-scoping variables and functions using `let` instead of `var`, arrow functions, lexical `this`, enhanced object properties, modules and, most of all, promises.

E.g.:

Use arrow functions:

	let fn = () => {
		return 2;
	};

For longer running actions, always use promises:

	let complexFn = () => {
		return new Promise((resolve, reject) => {
			let x = 7;
			// complex processing
			if (!error) {
				resolve(x);
			} else {
				reject(error);
			}
		});
	};

## Structure

Most files are to be organized as following:

	index.html
	js/
		app.js
		business.js
		(further JS files)
	img/
		(needed image files, if any)
	html/
		(further HTML files, if any)

The entry point to the applcation is `index.html` which will load and run all needed functionality. The logic is separated into UI (`app.js`) and business layer (`business.js`).

## Paper

The required paper has to be submitted until end of January 2019 in form of short report explaining the technology used and problems encountered.

