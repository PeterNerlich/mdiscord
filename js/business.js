
class BusinessLayer {
	constructor (client) {
		// init instance with discordJS client object
		this.client = client;
		this._storageAvailable = typeof(Storage) !== "undefined";
		this._channelListener = [];
	}

	_startMsgListener () {
		this.client.on('message', msg => {
			this._channelListener.forEach(e => {
				if (!e.id || e.id == msg.channel.id) {
					e.cb(msg);
				}
			});
		});
	}

	get storageAvailable () {
		return this._storageAvailable;
	}

	get token () {
		// get current token
		// if no token in quick access, retrieve it from localstorage
		//	(this.recoverToken() does NOT return the token but a promise - app needs to implement support for this)
		return this._token || this.recoverToken();
	}
	set token (t) {
		// set new token
		// token is only saved to quick access, not localstorage (quick access will be flushed to localstorage on successful login)
		this._token = t;
	}

	recoverToken () {
		return new Promise((res, rej) => {
			if (!this.storageAvailable) {
				rej('Storage not available');
				return;
			}
			// get token from localstorage
			try {
				// save to this._token
				this._token = localStorage.getItem("savedToken");
				res(this._token);
			} catch (e) {
				rej(e);
			}
		})
	}

	saveToken (token) {
		// token parameter is optional. if provided, set token
		if (token) { this._token = token; }
		return new Promise((res, rej) => {
			if (!this.storageAvailable) {
				rej('Storage not available');
				return;
			}
			// save token to localstorage
			try {
				res(localStorage.setItem("savedToken", this._token));
			} catch (e) {
				rej(e);
			}
		})
	}

	clearToken () {
		return new Promise((res, rej) => {
			if (!this.storageAvailable) {
				rej('Storage not available');
				return;
			}
			try {
				res(localStorage.removeItem("savedToken"));
			} catch (e) {
				rej(e);
			}
		});
	}

	loginWithToken (token) {
		// token parameter is optional. if provided, set token
		if (token) { this._token = token; }
		// login with a token
		let pLogin = this.client.login(this._token);
		pLogin.then((() => {
			if (this.storageAvailable) {
				// save token to localstorage if successful
				return this.saveToken(this._token);
			}
		}).bind(this));	// bind(this) binds the context of this BusinessLayer instance to the function, this.saveToken would otherwise be expected in the instance of Promise
		return pLogin;
	}

	logout () {
		let c = this.client.destroy();
		this.client = null;
		return c;
	}

	send (channel, msg) {
		channel.send(msg)
			.then(message => console.log(`Sent message: ${message.content}`))
  			.catch(console.error);
	}

	readUser () {
		return new Promise((res, rej) => {
			res(this.client.user);
		});
	}

	readGuildList () {
		return new Promise((res, rej) => {
			res(this.client.guilds);
		});
	}

	readChannel (id, guild) {
		return new Promise((res, rej) => {
			res(this.client.guilds[id]);
		});
	}

	onMsg (cb) {
		let o = {id: null, cb};
		this._channelListener.push(o);
		this._startMsgListener();
		return (() => {
			this._channelListener.splice(this._channelListener.indexOf(o), 1);
		}).bind(this);
	}

	onChannelMsg (channel, cb) {
		let o = {id: channel.id, cb};
		this._channelListener.push(o);
		this._startMsgListener();
		return (() => {
			this._channelListener.splice(this._channelListener.indexOf(o), 1);
		}).bind(this);
	}
}
