window.addEventListener('load', () => {
	// on load, setup the various components (mostly event listeners)
	setupLogin();
	setupNavigation();
	setupChannel();
});


// shorthands for query selector
const qs = document.querySelector.bind(document);
const qsa = document.querySelectorAll.bind(document);

// create business layer instance (need to provide a discordJS client instance)
const bus = new BusinessLayer(new Discord.Client());


let currentChannel = null;
let cancelChannelListener = null;


function setupLogin() {
	// bind login function to form submit event (button click, [enter] in text field etc.)
	qs('#auth form').addEventListener('submit', e => {
		e.preventDefault();
		return login();
	});
	// try to recover saved token
	bus.recoverToken()
		.then(autologin)	// try to login
		.catch(()=>{});	// do nothing if unsuccessful
}

function setupNavigation() {
	client = qs('#navigation .client');

	client.querySelector('[data-a="myaccount"]').addEventListener('click', e => {
		qs('html').classList.remove('showNav');
		qs('html').classList.add('showUserinfo');
	});
	client.querySelector('[data-a="logout"]').addEventListener('click', e => {
		bus.logout();
		bus.clearToken();
		qs('html').classList.remove('showNav');
		qs('html').classList.add('showAuth');
	});;
	client.querySelectorAll('[data-a="link"]').forEach(e => {
		e.addEventListener('click', e => {
			window.open((e.target || e.srcElement).getAttribute('data-href'));
		});
	});
}

function setupChannel() {
	qs('#channel .nav').addEventListener('click', e => {
		qs('html').classList.remove('showChannel');
		qs('html').classList.add('showNav');
	});

	qs('#channel .input form').addEventListener('submit', e => {
		e.preventDefault();
		bus.send(currentChannel, qs('#channel .input input').value);
		qs('#channel .input input').value = '';
	});
}




function login() {
	// disable form elements until server response
	qsa('#auth form input, #auth form button').forEach(e => {
		e.disabled = true;
	});
	// login
	return bus.loginWithToken( qs('#auth form input[name="token"]').value )
		.then(loginSuccessful)
		.catch(loginUnsuccessful);
}

function loginSuccessful() {
	// yay! change the view by modifying the html classes. the CSS will do the rest
	qs('html').classList.remove('showAuth');
	//qs('html').classList.add('showUserinfo');
	qs('html').classList.add('showNav');
	updateUserinfo();
	updateNavigation();

	// enable form elements again (for in case we come back to the auth view)
	qsa('#auth form input, #auth form button').forEach(e => {
		e.disabled = false;
	});
}

function updateUserinfo(){
	bus.readUser().then(user => {
		user.mobile = true;
		//qs('#userinfo .username').innerText = user.username;
		qs('#userinfo .tag').innerText = user.tag;
		qs('#userinfo .email').innerText = user.email;
		qs('#userinfo .avatar').src = user.displayAvatarURL;
	})
}

function loginUnsuccessful(reason) {
	// re-enable form elements to try again
	qsa('#auth form input, #auth form button').forEach(e => {
		e.disabled = false;
	});
	// TODO: error notification
}

function autologin(token) {
	// enter the token
	qs('#auth form input[name="token"]').value = token;
	// run normal login procedure
	return login();
}

function updateNavigation() {
	qs('#navigation .guilds').innerHTML = '';

	bus.readGuildList().then(guildCollection => {
		guildCollection.forEach(e => {
			let li = document.createElement('li');
			li.innerText = e.name;
			li.addEventListener('click', updateChannelNav.bind(this, e));
			qs('#navigation .guilds').appendChild(li);
		});
	})
}

function updateChannelNav(guild) {
	qs('#navigation .channels').innerHTML = '';
	
	guild.channels.forEach(e => {
		if (e.type == 'text' || e.type == 'voice'){
			let li = document.createElement('li');
			li.innerText = e.name;
			li.classList.add(e.type);
			if (e.type == 'text'){
				li.addEventListener('click', () => {
					qs('html').classList.remove('showNav');
					qs('html').classList.add('showChannel');
					currentChannel = e;
					updateChannel();
				});
			}
			qs('#navigation .channels').appendChild(li);
		}
	});
}

function addMessage(msg) {
	let m = document.createElement('div');
	m.id = msg.id;

	let author = document.createElement('div');
	author.classList.add('author');
	let img = document.createElement('div');
	img.classList.add('avatar');
	img.src = '';
	author.appendChild(img);
	let h3 = document.createElement('h3');
	h3.innerText = msg.author.tag;
	let time = document.createElement('time');
	if (msg.createdAt > (d => {d.setDate(d.getDate()-1); return d;})(new Date())) {
		// at most 24h old, display time
		time.innerText = msg.createdAt.toLocaleTimeString();
	} else {
		// more than 24h old, display date
		time.innerText = msg.createdAt.toLocaleDateString();
	}
	h3.appendChild(time);
	author.appendChild(h3);
	m.appendChild(author);

	let content = document.createElement('div');
	content.classList.add('content');
	let p = document.createElement('p');
	p.innerText = msg.cleanContent; // TODO: use .content and parse all mentions, markdown etc.
	content.appendChild(p);
	m.appendChild(content);

	qs('#channel .log').appendChild(m);
}

function updateChannel() {
	if (cancelChannelListener) {
		cancelChannelListener();
	}

	qs('#channel .name').innerText = '#'+currentChannel.name;
	qs('#channel .topic').innerText = currentChannel.topic || '';
	qs('#channel .input input').placeholder = 'Message #'+currentChannel.name+'...';

	qs('#channel .log').innerHTML = '';

	currentChannel.fetchMessages({limit: 20}).then(messages => {
		qs('#channel .log').innerHTML = '';
		messages.sort((a,b) => a.createdTimestamp-b.createdTimestamp).forEach(addMessage);
		scrollToBottom();
	}).catch(e => {
		qs('#channel .log').innerText = 'Error loading channel messages: '+JSON.stringify(e);
	});

	cancelChannelListener = bus.onChannelMsg(currentChannel, msg => {
		addMessage(msg);

		var scroll = document.body.scrollTop + document.body.clientHeight >= document.body.scrollHeight;
		if (scroll) {
			scrollToBottom();
		}
	});
}



function scrollToBottom(d) {
	if (d) setTimeout(scrollToBottom(), d);
	return window.scrollTo(0, document.body.scrollHeight);
}
