# Use cases:

## First Login (token based, successfull)

- user visits app page 
- app displays login dialog
- user enters token
- app disables login form
- app logs into discord api using token
- server responds: token valid
- app hides login dialog
- app saves token to localstorage
- app displays guild/channel list

## First Login (token based, token invalid)

- user visits app page
- app displays login dialog
- user enters invalid token or no token
- app logs into discord api using token
- server responds: token invalid, login refused
- app displays "login failed" notification, re-enables login form

## Automatic login (token based, successfull)

- user visits app page
- app detects saved token in localstorage
- app disables login form
- app logs into discord api using token
- server responds: token valid
- app hides login dialog
- app displays guild/channel list

## Automatic login (token based, token invalid)

- user visits app page
- app detects saved token in localstorage
- app logs into discord api using token
- server responds: token invalid, login refused
- app displays "login failed" notification, re-enables login form
- app deletes invalid token in localstorage

## Read messages

- user selects the desired guild/channel
- app hides guild/channel list
- app displays message history and input field
- (app indicates oldest unread message)

## Send text/md messages

- user clicks into input field
- user types message (can include markdown sequences)
- user presses enter
- app submits message to server
- app updates channel view to include sent message

## New message (in currently selected channel)

- message appears in message history

## New message (in other channel)

- if guild/channel is not muted, app prompts notification

## New direct message (in currently selected direct channel)

- message appears in message history
- app prompts notification

## New direct message (in other direct channel)

- app prompts notification

## New mention (in currently selected channel)

- message appears highlighted in message history
- app prompts notification

## New mention (in other channel)

- app prompts notification

## Logout

- user clicks logout button
- app hides chat/menu dialog and messages
- app disconnects from discord api
- app clears token from localstorage
- app displays login dialog
